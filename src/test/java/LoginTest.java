import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.swing.*;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class LoginTest {
    WebDriver driver;

    @BeforeClass //Dijalankan sebelum eksekusi semua method yang ada di dalam class tersebut
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();///Resize current window to the set dimension
        //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        /*implicitWait This timeout is used to specify the amount of time the driver
        should wait while searching for an element if it is not immediately present.*/
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
    }
    @AfterMethod//Dieksekusi setelah setiap method dijalankan
    public void logout() throws InterruptedException {
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/authenticate.php?logout");
        Thread.sleep(1000);
    }

    @Test(priority = 1)
    public void TC001(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");//url halaman login
        //ambil value username & password
        String username= driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[1]/div/div/input")).getAttribute("value");
        String password=driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[2]/form/div[1]/div[2]/div/div/input")).getAttribute("value");
        // login

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"appointment\"]/div/div/div/h2")).getText(), "Make Appointment");
    }

    @Test(priority = 2)
    public void TC002(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");//url halaman login

        // login

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("John Doe");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("WrongPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");

    }

    @Test(priority = 3)
    public void TC003(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");//url halaman login

        // login

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("Doe Jhon");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("ThisIsNotAPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");

    }

    @Test(priority = 4)
    public void TC004(){
        driver.navigate().to("https://katalon-demo-cura.herokuapp.com/profile.php#login");//url halaman login

        // login

        driver.findElement(By.xpath("//*[@id=\"txt-username\"]")).sendKeys("Doe Jhon");
        driver.findElement(By.xpath("//*[@id=\"txt-password\"]")).sendKeys("WrongPassword");
        driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div[1]/p[2]")).getText(), "Login failed! Please ensure the username and password are valid.");

    }


    @AfterClass
    public void classBrowser() throws InterruptedException{
        Thread.sleep(2000);
        //Close the browser
        driver.quit();
    }


}
